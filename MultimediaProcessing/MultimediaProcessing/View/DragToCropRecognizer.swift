//
//  DragToCropRecognizer.swift
//  MultimediaProcessing
//
//  Created by Phong Ngo Phu on 1/13/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit.UIGestureRecognizerSubclass

class DragToCropRecognizer: UIGestureRecognizer {
    var origin = CGPoint()
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        
        state = .began
        for touch in touches {
            if touch.phase == .began {
                origin = touch.location(in: self.view)
                break
            }
            break
        }
        super.touchesBegan(touches, with: event)
        
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesEnded(touches, with: event)
        state = .ended
    }
}
