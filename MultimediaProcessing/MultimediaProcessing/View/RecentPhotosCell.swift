//
//  RecentPhotosCell.swift
//  MultimediaProcessing
//
//  Created by Phong Ngo Phu on 1/8/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit

class RecentPhotosCell: UITableViewCell {
    
    @IBOutlet weak var recentImageView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        recentImageView.layer.cornerRadius = 10
        recentImageView.layer.borderColor = UIColor.black.cgColor
        recentImageView.layer.borderWidth = 0.45
        recentImageView.layer.masksToBounds = true
    }
    func configureCell(name : String, image: UIImage) {
        nameLbl.text = name
        recentImageView.image = image
    }
}
