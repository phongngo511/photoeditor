//
//  CropImageView.swift
//  MultimediaProcessing
//
//  Created by Phong Ngo Phu on 1/13/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit

class CropImageView: UIImageView {

    var dragOrigin = CGPoint()
    var cropView = UIView()
    var dragGesture = DragToCropRecognizer()
    var hideGesture = UITapGestureRecognizer()
//    var moveGesture = UIPanGestureRecognizer()
    
    // The first touchpoint when the user began moving the cropView
    var touchOrigin = CGPoint()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeInstance()
    }
    override init(image: UIImage?) {
        super.init(image: image)
        initializeInstance()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeInstance()
    }
    
    func initializeInstance() {
        dragOrigin = CGPoint.zero
        self.isUserInteractionEnabled = true
        cropView = UIView(frame: CGRect(x: -1.0, y: -1.0, width: 0.0, height: 0.0))
        cropView.isHidden = true
        cropView.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1)
        cropView.alpha = 0.5
        cropView.layer.borderWidth = 1.0
        cropView.layer.borderColor = #colorLiteral(red: 0.1215686275, green: 0.1294117647, blue: 0.1411764706, alpha: 1)
        self.addSubview(cropView)
        
        dragGesture = DragToCropRecognizer(target: self, action: #selector(dragRectangle(gesture:)))
        hideGesture = UITapGestureRecognizer(target: self, action: #selector(hideCropRectangle(gesture:)))
//        moveGesture = UIPanGestureRecognizer(target: self, action: #selector(moveRectangle(gesture:)))
        
        self.addGestureRecognizer(dragGesture)
        self.addGestureRecognizer(hideGesture)
//        cropView.addGestureRecognizer(moveGesture)
    }
    
    @objc func dragRectangle(gesture: DragToCropRecognizer) {
        var cropRect = CGRect.zero
        if gesture.state == .began {
            cropView.isHidden = false
            dragOrigin = gesture.origin
            cropRect.origin = gesture.origin
        } else {
            cropRect = cropView.frame
            let currentPoint = gesture.location(in: self)
            if currentPoint.x >= dragOrigin.x && currentPoint.y >= dragOrigin.y {
                cropRect.origin = dragOrigin
                cropRect.size = CGSize(width: currentPoint.x - cropRect.origin.x, height: currentPoint.y - cropRect.origin.y)
            } else if currentPoint.x <= dragOrigin.x && currentPoint.y <= dragOrigin.y {
                cropRect.origin = currentPoint
                cropRect.size = CGSize(width: dragOrigin.x - currentPoint.x, height: dragOrigin.y - currentPoint.y)
            } else if currentPoint.x < dragOrigin.x {
                cropRect.origin = CGPoint(x: currentPoint.x, y: dragOrigin.y)
                cropRect.size = CGSize(width: dragOrigin.x - currentPoint.x, height: currentPoint.y - dragOrigin.y)
            } else if currentPoint.y < dragOrigin.y {
                cropRect.origin = CGPoint(x: dragOrigin.x, y: currentPoint.y)
                cropRect.size = CGSize(width: currentPoint.x - dragOrigin.y, height: dragOrigin.y - currentPoint.y)
            }
        }
        cropView.frame = cropRect
        
    }
    @objc func hideCropRectangle(gesture: UITapGestureRecognizer) {
        if !cropView.isHidden {
            cropView.isHidden = true
            cropView.frame = CGRect(x: -1.0, y: -1.0, width: 0.0, height: 0.0)
        }
    }
//    @objc func moveRectangle(gesture: UIPanGestureRecognizer) {
//        if gesture.state == UIGestureRecognizerState.began {
//            touchOrigin = gesture.location(in: self)
//            dragOrigin = cropView.frame.origin
//        } else {
//            let dx,dy:CGFloat
//            let currentPt: CGPoint = gesture.location(in: self)
//            dx = currentPt.x - touchOrigin.x
//            dy = currentPt.y - touchOrigin.y
//            cropView.frame = CGRect(x: dragOrigin.x + dx, y: dragOrigin.y + dy, width: cropView.frame.size.width, height: cropView.frame.size.height)
//        }
//    }
    
    func crop() -> UIImage {
        if cropView.isHidden {
            return UIImage()
        }
        var croppedImage = UIImage()
        if let img = self.image {
            croppedImage = img.cropRectangle(cropRect: cropView.frame, inFrame: self.frame.size)
        }
        cropView.isHidden = true
        return croppedImage
    }
    
}
