//
//  UIImageView+Extension.swift
//  MultimediaProcessing
//
//  Created by Phong Ngo Phu on 1/10/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit

extension UIImageView {
    // Cause of image's contentMode as aspect Fit, it may not fill all the space of ImageVIew
    // This func to get actual Frame of image
    func toImageFrame() -> CGRect {
        let imageViewSize = self.frame.size
        guard let imageSize = self.image?.size else { return CGRect.zero }
        
        let imageRatio = imageSize.width / imageSize.height
        let imageViewRatio = imageViewSize.width / imageViewSize.height
        
        if imageRatio < imageViewRatio {
            
            let scaleFactor = imageViewSize.height / imageSize.height
            let width = imageSize.width * scaleFactor
            
            let topLeftX = ( imageViewSize.width - width ) / 2
            return CGRect(x: topLeftX, y: 0, width: width, height: imageViewSize.height)
        } else {
            let scaleFactor = imageViewSize.width / imageSize.width
            let height = imageSize.height * scaleFactor
            let topLeftY = (imageViewSize.height - height ) / 2
            return CGRect(x: 0, y: topLeftY, width: imageViewSize.width, height: height)
        }
    }
}
