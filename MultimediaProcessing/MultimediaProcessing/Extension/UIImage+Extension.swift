//
//  UIImage+Extension.swift
//  MultimediaProcessing
//
//  Created by Phong Ngo Phu on 1/11/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit
import CoreImage

extension UIImage {
    func updateImageAfterTransform(withTransform transform:CGAffineTransform ,imageView: UIImageView, image: UIImage) -> UIImage {
        let imageViewRatio = imageView.bounds.size.width / imageView.bounds.size.height
        let imageRatio = image.size.width / image.size.height
        
        var canvasSize = image.size
        
        if imageViewRatio > imageRatio {
            canvasSize.width =  canvasSize.height * imageViewRatio
        } else {
            canvasSize.height =  canvasSize.width / imageViewRatio
        }
        
        let xScale = canvasSize.width / imageView.bounds.width
        let yScale = canvasSize.height / imageView.bounds.height
        
        let center = __CGPointApplyAffineTransform(imageView.center,  CGAffineTransform.identity.scaledBy(x: xScale, y: yScale))
        
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, 0);
        let context = UIGraphicsGetCurrentContext()!
        
        // Apply transformation
        context.translateBy(x: center.x, y: center.y)
        context.concatenate(transform)
        context.translateBy(x: -center.x, y: -center.y)
        
        var drawingRect : CGRect = CGRect.zero
        drawingRect.size = canvasSize
        
        // Transaltion
        drawingRect.origin.x = (center.x - size.width / 2)
        drawingRect.origin.y = (center.y - size.height / 2)
        
        // Aspectfit calculation
        if imageViewRatio > imageRatio {
            drawingRect.size.width =  drawingRect.size.height * imageRatio
        } else {
            drawingRect.size.height = drawingRect.size.width / imageRatio
        }
        
        image.draw(in: drawingRect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
    }

    
    // Mark: - Crop Functions
    
    func cropRectangle(cropRect: CGRect, inFrame frameSize: CGSize) -> UIImage {
        var size = frameSize
        size = CGSize(width: round(size.width), height: size.height)
        
        // Resize image to match the zooomed content size
        let img = scaleBitmapToSize(scaleSize: size)
        
        // Crop the resized image to the crop rectangle
        let cropRef = img.cgImage!.cropping(to: cropRect)!
        let image = UIImage(cgImage: cropRef, scale: 1.0, orientation: self.imageOrientation)
        let croppedImg = UIImage(cgImage: image.cgImage!, scale: 1.0, orientation: self.imageOrientation)
        return croppedImg
    }
    
    func scaleSize(fromSize: CGSize, to toSize: CGSize) -> CGSize {
        
        var scaleSize = CGSize.zero
        // if the width is the shorter dimension
        if toSize.width < toSize.height {
            if fromSize.width >= toSize.width { // give priority to width if it is larger than the destination width
                scaleSize.width = round(toSize.width)
                scaleSize.height = round(scaleSize.width * fromSize.height / fromSize.width)
            } else if fromSize.height >= toSize.height { // then give priority to height if it is larger than destination height
                scaleSize.height = round(toSize.height)
                scaleSize.width = round(scaleSize.height * fromSize.width / fromSize.height )
            } else { // otherwise the source size is smaller in all directions.  Scale on width
                scaleSize.width = round(toSize.width)
                scaleSize.height = round(scaleSize.width * fromSize.height / fromSize.width )
                if scaleSize.height > toSize.height {
                    scaleSize.height = round(toSize.height)
                    scaleSize.width = round(scaleSize.height * fromSize.width / fromSize.height )
                }
            }
        } else { // else height is the shorter dimension
            if (fromSize.height >= toSize.height) {  // then give priority to height if it is larger than destination height
                scaleSize.height = round(toSize.height);
                scaleSize.width = round(scaleSize.height * fromSize.width / fromSize.height);
            } else if (fromSize.width >= toSize.width) {  // give priority to width if it is larger than the destination width
                scaleSize.width = round(toSize.width);
                scaleSize.height = round(scaleSize.width * fromSize.height / fromSize.width);
            } else {  // otherwise the source size is smaller in all directions.  Scale on width
                scaleSize.width = round(toSize.width);
                scaleSize.height = round(scaleSize.width * fromSize.height / fromSize.width);
                if (scaleSize.height > toSize.height) { // but if the new height is larger than the destination then scale height
                    scaleSize.height = round(toSize.height);
                    scaleSize.width = round(scaleSize.height * fromSize.width / fromSize.height);
                }
            }
        }
        return scaleSize
    }
    
    func scaleBitmapToSize(scaleSize: CGSize) -> UIImage {
        var size = scaleSize
        size = CGSize(width: round(size.width), height: round(size.height))
        
        let cgImage = self.cgImage!
        // Create bitmap context and draw underlying CGimage into context
        let context = CGContext.init(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: cgImage.colorSpace!, bitmapInfo: cgImage.bitmapInfo.rawValue)!
        
        var returnImg = UIImage()
        
        context.draw(cgImage, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let imgRef = context.makeImage()!
        returnImg = UIImage(cgImage: imgRef)
        
        return returnImg
    }
    
    
    func resizeImage(targetSize: CGSize) -> UIImage {
        let size = self.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

extension UIImage {
    // Mark : - Functions that's useless For now
    func rotateImage(withRadian radians: CGFloat) -> UIImage {
        let cgImage = self.cgImage!
        let maxSize = CGFloat(max(self.size.width, self.size.height))
        let context = CGContext.init(data: nil, width: Int(maxSize), height: Int(maxSize), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: cgImage.colorSpace!, bitmapInfo: cgImage.bitmapInfo.rawValue)!

        var drawRect = CGRect.zero
        drawRect.size = self.size
        let drawOrigin = CGPoint(x: ( maxSize - self.size.width ) / 2, y: ( maxSize - self.size.height ) / 2)
        drawRect.origin = drawOrigin

        var tf = CGAffineTransform.identity
        tf = tf.translatedBy(x: maxSize / 2, y: maxSize / 2)
        tf = tf.rotated(by: CGFloat(radians))
        tf = tf.translatedBy(x: -maxSize / 2, y: -maxSize / 2)
        context.concatenate(tf)
        context.draw(cgImage, in: drawRect)

        var roatedImage = context.makeImage()!
        drawRect = drawRect.applying(tf)
        roatedImage = roatedImage.cropping(to: drawRect)!
        let resultImage = UIImage(cgImage: roatedImage)
        return resultImage
    }
//    func zoomImage(atPoint origin: CGPoint, img: UIImage, zoomScale: CGFloat) -> UIImage {
//        UIGraphicsBeginImageContext(img.size)
//        let context = UIGraphicsGetCurrentContext()!
//
//        var drawRect = CGRect.zero
//        drawRect.size = img.size
//        drawRect.origin = origin
//
//        var tf = CGAffineTransform.identity
//        tf = tf.scaledBy(x: zoomScale, y: zoomScale)
//        context.concatenate(tf)
//        context.draw(img.cgImage!, in: drawRect)
//
//        var zoomedInImage = context.makeImage()!
//        drawRect = drawRect.applying(tf)
//        zoomedInImage = zoomedInImage.cropping(to: drawRect)!
//        let resultImg = UIImage(cgImage: zoomedInImage)
//        UIGraphicsEndImageContext()
//        return resultImg
//    }
//
//    func screenshotImage(imageContainer: UIImageView) -> UIImage{
//        UIGraphicsBeginImageContextWithOptions(imageContainer.bounds.size, false, 0)
//        imageContainer.snapshotView(afterScreenUpdates: true)
//        let screenshot = UIGraphicsGetImageFromCurrentImageContext()!
//        UIGraphicsEndImageContext()
//        return screenshot
//    }
//
//    func scaleImageToSize(newSize: CGSize) -> UIImage {
//        var scaledImageRect = CGRect.zero
//
//        let aspectWidth = newSize.width/size.width
//        let aspectheight = newSize.height/size.height
//
//        let aspectRatio = max(aspectWidth, aspectheight)
//
//        scaledImageRect.size.width = size.width * aspectRatio;
//        scaledImageRect.size.height = size.height * aspectRatio;
//        scaledImageRect.origin.x = (newSize.width - scaledImageRect.size.width) / 2.0;
//        scaledImageRect.origin.y = (newSize.height - scaledImageRect.size.height) / 2.0;
//
//        UIGraphicsBeginImageContext(newSize)
//        draw(in: scaledImageRect)
//        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//
//        return scaledImage!
//    }
//
//    func scaled(withScale scale: CGFloat) -> UIImage? {
//
//        let size = CGSize(width: self.size.width * scale, height: self.size.height * scale)
//
//        UIGraphicsBeginImageContextWithOptions(size, false, 0)
//
//        draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
//
//        let image = UIGraphicsGetImageFromCurrentImageContext()
//
//        UIGraphicsEndImageContext()
//
//        return image
//    }
}

