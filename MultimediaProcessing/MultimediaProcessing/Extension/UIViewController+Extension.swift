//
//  UIViewController+Extension.swift
//  MultimediaProcessing
//
//  Created by Phong Ngo Phu on 1/10/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit

protocol ImagePassingDelegate {
    func passImage(img: UIImage)
}
protocol RecentImagePassingDelegate {
    func passRecentImage(img: UIImage, date: String)
}

extension UIViewController {
    func presentDetail(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        self.view.window?.layer.add(transition, forKey: kCATransition)
        
        present(viewControllerToPresent, animated: false, completion: nil)
    }
    
    func dismissDetail() {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        self.view.window?.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: false, completion: nil)
    }

}
