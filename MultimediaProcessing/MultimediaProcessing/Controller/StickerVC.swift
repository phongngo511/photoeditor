//
//  StickerVC.swift
//  MultimediaProcessing
//
//  Created by Phong Ngo Phu on 3/6/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit

class StickerVC: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var stickerImageView: UIImageView!
    @IBOutlet weak var stickerScrollView: UIScrollView!
    
    var passedImage: UIImage!
    var delegateImg : ImagePassingDelegate!
    
    var dataArray = [UIImage]()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainImageView.image = passedImage
        for i in 1...83 {
            dataArray.append(UIImage(named: "\(i)")!)
        }
        addGesture()
        addStickerButton()
    }
    func addStickerButton() {
        
        var xCoord: CGFloat = 5
        let yCoord: CGFloat = 5
        let buttonWidth:CGFloat = 70
        let buttonHeight: CGFloat = 50
        let gapBetweenButtons: CGFloat = 5
        var itemCount = 0
        
        for i in 0..<dataArray.count {
            itemCount = i
            
            // Button properties
            let stickerButton = UIButton(type: .custom)
            stickerButton.frame = CGRect(x: xCoord, y: yCoord, width: buttonWidth, height: buttonHeight)
            stickerButton.tag = itemCount
            stickerButton.showsTouchWhenHighlighted = true
            stickerButton.addTarget(self, action: #selector(stickerButtonTapped(sender:)), for: .touchUpInside)
            stickerButton.layer.cornerRadius = 5
            stickerButton.clipsToBounds = true
            
            stickerButton.setBackgroundImage(dataArray[i], for: .normal)
            // Add Buttons in the Scroll View
            xCoord +=  buttonWidth + gapBetweenButtons
            stickerScrollView.addSubview(stickerButton)
        }
        stickerScrollView.contentSize = CGSize(width: buttonWidth * CGFloat(itemCount + 1), height: yCoord)
    }
    @objc func stickerButtonTapped(sender: UIButton) {
        stickerImageView.image = sender.backgroundImage(for: .normal)
    }

    func addGesture() {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(gesture:)))
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(handlePinchGesture(gesture:)))
        let rotate = UIRotationGestureRecognizer(target: self, action: #selector(handleRotateGesture(gesture:)))
        stickerImageView.addGestureRecognizer(pan)
        stickerImageView.addGestureRecognizer(pinch)
        stickerImageView.addGestureRecognizer(rotate)
    }
    
    @objc func handlePinchGesture(gesture: UIPinchGestureRecognizer) {
        let senderView = gesture.view as! UIImageView
        let scale = gesture.scale
        senderView.transform = senderView.transform.scaledBy(x: scale, y: scale)
        if senderView.transform.a > 2.0 {
            senderView.transform.a = 2.0    // x coordinate
            senderView.transform.d = 2.0    // x coordinate
        }
        if senderView.transform.d < 0.4 {
            senderView.transform.a = 0.4
            senderView.transform.d = 0.4
        }
        gesture.scale = 1
    }
    @objc func handleRotateGesture(gesture: UIRotationGestureRecognizer) {
        let rotation = gesture.rotation
        let senderView = gesture.view as! UIImageView
        senderView.transform = senderView.transform.rotated(by: rotation)
        gesture.rotation = 0
    }
    @objc func handlePanGesture(gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: self.mainImageView)
        let senderView = gesture.view as! UIImageView
        
        var dx = senderView.center.x + translation.x
        var dy = senderView.center.y + translation.y
        if dy < mainImageView.frame.origin.y {
            dy = mainImageView.frame.origin.y
        }
        if dy > mainImageView.frame.maxY {
            dy = mainImageView.frame.maxY
        }
        if dx < mainImageView.frame.origin.x {
            dx = mainImageView.frame.origin.x
        }
        if dx > mainImageView.frame.maxX {
            dx = mainImageView.frame.maxX
        }
        senderView.center = CGPoint(x: dx, y: dy)
        gesture.setTranslation(CGPoint.zero, in: self.mainImageView)
    }
    
    @IBAction func tickBtnPressed(_ sender: UIButton) {
        mainImageView.image = viewToImage(drawTextAndImageIn: mainView)
        
        delegateImg.passImage(img: mainImageView.image!)
        dismiss(animated: false, completion: nil)
    }
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        delegateImg.passImage(img: passedImage)
        dismiss(animated: false, completion: nil)
    }
    func viewToImage(drawTextAndImageIn view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, 0)
        
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
