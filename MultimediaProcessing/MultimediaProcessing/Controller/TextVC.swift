//
//  ViewController.swift
//  MultimediaProcessing
//
//  Created by Phong Ngo Phu on 1/7/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit

class TextVC: UIViewController {

    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var textTV: UITextView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var colorPickerView: ColorPickerView!
    @IBOutlet weak var opacitySlider: UISlider!
    @IBOutlet weak var editTextStackView: UIStackView!
    
    @IBOutlet weak var fontTF: UITextField!
    @IBOutlet weak var alignmentTF: UITextField!
    @IBOutlet weak var styleTF: UITextField!
    
    var passedImage: UIImage!
    var delegateImg : ImagePassingDelegate!
    
    var fontArray = ["Helvetica Neue"]
    var fontPicker = UIPickerView()
    var selectedFont = "Helvetica Neue"
    
    let alignmentArray = ["Center", "Left", "Right", "Natural", "Justified"]
    var alignmentPicker = UIPickerView()
    var selectedAlignment = "Center"
    
    let styleArray = ["Bold", "Italic", "Normal"]
    var stylePicker = UIPickerView()
    var selectedStyle = "Normal"
    
    var selectedPicker: UIPickerView?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for familyName in UIFont.familyNames {
            fontArray.append(familyName)
        }
        
        mainImageView.image = passedImage
        textTV.delegate = self
        colorPickerView.delegate = self
        
        addGesture()
        setupPicker()
    }
    
    func setupPicker() {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.barTintColor = #colorLiteral(red: 0.3557463515, green: 0.3557463515, blue: 0.3557463515, alpha: 1)
        
        let pickerHeight:Int = 216
        let viewWidth = view.frame.size.width
        let pickerFrame = CGRect(x: 0, y: pickerHeight, width: Int(viewWidth), height: pickerHeight)
        
        fontPicker.frame = pickerFrame
        fontPicker.dataSource = self
        fontPicker.delegate = self
        fontPicker.showsSelectionIndicator = true
        fontPicker.backgroundColor = #colorLiteral(red: 0.1954116434, green: 0.1954116434, blue: 0.1954116434, alpha: 1)
        fontTF.inputView = fontPicker
        fontTF.textAlignment = .center
        fontTF.inputAccessoryView = toolBar
        
        alignmentPicker.frame = pickerFrame
        alignmentPicker.dataSource = self
        alignmentPicker.delegate = self
        alignmentPicker.showsSelectionIndicator = true
        alignmentPicker.backgroundColor = #colorLiteral(red: 0.1954116434, green: 0.1954116434, blue: 0.1954116434, alpha: 1)
        alignmentTF.inputView = alignmentPicker
        alignmentTF.textAlignment = .center
        alignmentTF.inputAccessoryView = toolBar
        
        stylePicker.frame = pickerFrame
        stylePicker.dataSource = self
        stylePicker.delegate = self
        stylePicker.showsSelectionIndicator = false
        stylePicker.backgroundColor = #colorLiteral(red: 0.1954116434, green: 0.1954116434, blue: 0.1954116434, alpha: 1)
        styleTF.inputView = stylePicker
        styleTF.textAlignment = .center
        styleTF.inputAccessoryView = toolBar
        
    }
    
    func addGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(gesture:)))
        mainImageView.addGestureRecognizer(tap)
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(gesture:)))
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(handlePinchGesture(gesture:)))
        let rotate = UIRotationGestureRecognizer(target: self, action: #selector(handleRotateGesture(gesture:)))
        textTV.addGestureRecognizer(pan)
        textTV.addGestureRecognizer(pinch)
        textTV.addGestureRecognizer(rotate)
    }
    
    @objc func handlePinchGesture(gesture: UIPinchGestureRecognizer) {
        let senderView = gesture.view as! UITextView
        let scale = gesture.scale
        senderView.transform = senderView.transform.scaledBy(x: scale, y: scale)
        if senderView.transform.a > 2.0 {
            senderView.transform.a = 2.0    // x coordinate
            senderView.transform.d = 2.0    // x coordinate
        }
        if senderView.transform.d < 0.4 {
            senderView.transform.a = 0.4
            senderView.transform.d = 0.4
        }
        
        gesture.scale = 1
    }
    @objc func handleRotateGesture(gesture: UIRotationGestureRecognizer) {
        let rotation = gesture.rotation
        let senderView = gesture.view as! UITextView
        senderView.transform = senderView.transform.rotated(by: rotation)
        gesture.rotation = 0
    }
    @objc func handlePanGesture(gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: self.mainImageView)
        let senderView = gesture.view as! UITextView
        
        var dx = senderView.center.x + translation.x
        var dy = senderView.center.y + translation.y
        if dy < mainImageView.frame.origin.y {
            dy = mainImageView.frame.origin.y
        }
        if dy > mainImageView.frame.maxY {
            dy = mainImageView.frame.maxY
        }
        if dx < mainImageView.frame.origin.x {
            dx = mainImageView.frame.origin.x
        }
        if dx > mainImageView.frame.maxX {
            dx = mainImageView.frame.maxX
        }
        senderView.center = CGPoint(x: dx, y: dy)
        gesture.setTranslation(CGPoint.zero, in: self.mainImageView)
    }
    
    @objc func handleTapGesture(gesture: UITapGestureRecognizer) {
        textTV.endEditing(true)
    }
    
    @objc func donePicker() {
        if selectedPicker === fontPicker {
            textTV.font = UIFont(name: selectedFont, size: 17)
            fontTF.resignFirstResponder()
        }
        if selectedPicker === alignmentPicker {
            if selectedAlignment == "Center" {
                textTV.textAlignment = .center
                alignmentTF.resignFirstResponder()
            }
            if selectedAlignment == "Left" {
                textTV.textAlignment = .left
                alignmentTF.resignFirstResponder()
            }
            if selectedAlignment == "Right" {
                textTV.textAlignment = .right
                alignmentTF.resignFirstResponder()
            }
            if selectedAlignment == "Natural" {
                textTV.textAlignment = .natural
                alignmentTF.resignFirstResponder()
            }
            if selectedAlignment == "Justified" {
                textTV.textAlignment = .justified
                alignmentTF.resignFirstResponder()
            }
        }
        if selectedPicker === stylePicker {
            if selectedStyle == "Bold" {
                textTV.font = UIFont.boldSystemFont(ofSize: 17)
                styleTF.resignFirstResponder()
            }
            if selectedStyle == "Italic" {
                textTV.font = UIFont.italicSystemFont(ofSize: 17)
                styleTF.resignFirstResponder()
            }
            if selectedStyle == "Normal" {
                textTV.font = UIFont.systemFont(ofSize: 17)
                styleTF.resignFirstResponder()
            }
        }
    }
    @objc func cancelPicker() {
        fontTF.resignFirstResponder()
        alignmentTF.resignFirstResponder()
        styleTF.resignFirstResponder()
    }
    
    @IBAction func tickBtnPressed(_ sender: UIButton) {
        textTV.endEditing(true)
        mainImageView.image = viewToImage(drawTextAndImageIn: mainView)
        
        delegateImg.passImage(img: mainImageView.image!)
        dismiss(animated: false, completion: nil)
    }
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        delegateImg.passImage(img: passedImage)
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func colorBtnPressed(_ sender: UIButton) {
        colorPickerView.isHidden = !colorPickerView.isHidden
        if !colorPickerView.isHidden {
            opacitySlider.isHidden = true
            editTextStackView.isHidden = true
        }
    }
    @IBAction func opacityBtnPressed(_ sender: UIButton) {
        opacitySlider.isHidden = !opacitySlider.isHidden
        if !opacitySlider.isHidden {
            colorPickerView.isHidden = true
            editTextStackView.isHidden = true
        }
    }
    @IBAction func editTextBtnPressed(_ sender: UIButton) {
        editTextStackView.isHidden = !editTextStackView.isHidden
        if !editTextStackView.isHidden {
            colorPickerView.isHidden = true
            opacitySlider.isHidden = true
        }
    }

    @IBAction func opacityDidChangeValue(_ sender: UISlider) {
        textTV.layer.opacity = sender.value
    }
    
    func viewToImage(drawTextAndImageIn view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, 0)
        
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
}
extension TextVC: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let size = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: textView.frame.size.height))
        if size.height > textView.frame.size.height {
            textView.frame.size.height = size.height
            textView.setContentOffset(CGPoint.zero, animated: false)
        }
    }
}
extension TextVC: ColorDelegate {
    func pickedColor(color: UIColor) {
        textTV.textColor = color
    }
}
extension TextVC: UIPickerViewDelegate  {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView === fontPicker { return fontArray[row] }
        if pickerView === alignmentPicker { return alignmentArray[row] }
        if pickerView === stylePicker { return styleArray[row] }
        return nil
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView === fontPicker {
            selectedFont = fontArray[row]
            selectedPicker = fontPicker
        }
        if pickerView === alignmentPicker {
            selectedAlignment = alignmentArray[row]
            selectedPicker = alignmentPicker
        }
        if pickerView === stylePicker {
            selectedStyle = styleArray[row]
            selectedPicker = stylePicker
        }
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var titleData = ""
        var fontName = "Helvetica Neue"
        if pickerView === fontPicker {
            titleData = fontArray[row]
            fontName = fontArray[row]
        }
        if pickerView === alignmentPicker { titleData = alignmentArray[row] }
        if pickerView === stylePicker { titleData = styleArray[row] }
        
        let pickerLabel = UILabel()
        pickerLabel.textColor = .white
        pickerLabel.text = titleData
        pickerLabel.font = UIFont(name: fontName, size: 20)
        pickerLabel.textAlignment = .center
        
        return pickerLabel
    }
}
extension TextVC: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView === fontPicker {
            return fontArray.count
        }
        if pickerView === alignmentPicker {
            return alignmentArray.count
        }
        if pickerView === stylePicker {
            return styleArray.count
        }
        return 0
    }
}
