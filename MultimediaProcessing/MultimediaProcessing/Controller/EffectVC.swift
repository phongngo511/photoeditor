//
//  EffectVC.swift
//  MultimediaProcessing
//
//  Created by Phong Ngo Phu on 3/6/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit

class EffectVC: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var effectImageView: UIImageView!
    @IBOutlet weak var effectScrollView: UIScrollView!
    
    var passedImage: UIImage!
    var delegateImg : ImagePassingDelegate!
    var dataArray = [UIImage]()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mainImageView.image = passedImage
        for i in 1...37 {
            dataArray.append(UIImage(named: "effect\(i)")!)
        }
        addEffectButton()
    }
    func addEffectButton() {
 
        var xCoord: CGFloat = 5
        let yCoord: CGFloat = 5
        let buttonWidth:CGFloat = 70
        let buttonHeight: CGFloat = 50
        let gapBetweenButtons: CGFloat = 5
        var itemCount = 0
        
        for i in 0..<dataArray.count {
            itemCount = i
            
            // Button properties
            let effectButton = UIButton(type: .custom)
            effectButton.frame = CGRect(x: xCoord, y: yCoord, width: buttonWidth, height: buttonHeight)
            effectButton.tag = itemCount
            effectButton.showsTouchWhenHighlighted = true
            effectButton.addTarget(self, action: #selector(stickerButtonTapped(sender:)), for: .touchUpInside)
            effectButton.layer.cornerRadius = 5
            effectButton.clipsToBounds = true
            
            effectButton.setBackgroundImage(dataArray[i], for: .normal)
            // Add Buttons in the Scroll View
            xCoord +=  buttonWidth + gapBetweenButtons
            effectScrollView.addSubview(effectButton)
        }
        effectScrollView.contentSize = CGSize(width: buttonWidth * CGFloat(itemCount + 4), height: yCoord)
    }
    @objc func stickerButtonTapped(sender: UIButton) {
        effectImageView.image = sender.backgroundImage(for: .normal)
    }
    @IBAction func tickBtnPressed(_ sender: UIButton) {
        mainImageView.image = viewToImage(drawTextAndImageIn: mainView)
        
        delegateImg.passImage(img: mainImageView.image!)
        dismiss(animated: false, completion: nil)
    }
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        delegateImg.passImage(img: passedImage)
        dismiss(animated: false, completion: nil)
    }
    func viewToImage(drawTextAndImageIn view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, 0)
        
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
