//
//  CropImageVC.swift
//  MultimediaProcessing
//
//  Created by Phong Ngo Phu on 1/14/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit

class CropImageVC: UIViewController {

    
    @IBOutlet weak var originalImageView: CropImageView!    
    @IBOutlet weak var tickBtn: UIButton!
    
    var delegateImg : ImagePassingDelegate!
    var passedImage = UIImage()
    var croppedImage = UIImage()
    var isImageCropped = false
    var currentImageSize = CGSize.zero
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        originalImageView.image = passedImage
        currentImageSize = passedImage.size
        tickBtn.isHidden = true
    }

    @IBAction func cropBtnPressed(_ sender: UIButton) {
        if originalImageView.cropView.isHidden == false {
            croppedImage = originalImageView.crop() // maybe UImage() empty
            originalImageView.image = croppedImage.resizeImage(targetSize: currentImageSize)
            isImageCropped = true
            tickBtn.isHidden = false
        }
        
    }
    
    @IBAction func closeBtnPressed(_ sender: UIButton) {
        if isImageCropped == false && tickBtn.isHidden == true {
            dismiss(animated: false, completion: nil)
        }
        originalImageView.image = passedImage
        tickBtn.isHidden = true
        isImageCropped = false
        
    }
    @IBAction func tickBtnPressed(_ segue: UIButton) {
        
        if isImageCropped {
            delegateImg.passImage(img: croppedImage)
            dismiss(animated: false, completion: nil)
        } else {
            dismiss(animated: false, completion: nil)
        }
        
    }
}
