//
//  CropVC.swift
//  MultimediaProcessing
//
//  Created by Phong Ngo Phu on 1/10/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit

class CropVC: UIViewController {
    
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var cropAreaView: UIView!

    var delegateImg : ImagePassingDelegate!
    var passedImage = UIImage()
    var cropArea: CGRect {
        get {
            let factor = mainImageView.image!.size.width / view.frame.width
            let imageFrame = mainImageView.toImageFrame()
            
            let x = (cropAreaView.frame.origin.x - imageFrame.origin.x) * factor
            let y = (cropAreaView.frame.origin.y - imageFrame.origin.y) * factor
            let width = cropAreaView.frame.size.width * factor
            let height = cropAreaView.frame.size.height * factor
            print(x)
            print(y)
            return CGRect(x: x, y: y, width: width, height: height)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        
    }

    func setupUI() {
        mainImageView.image = passedImage
        
        cropAreaView.layer.borderColor = UIColor.white.cgColor
        cropAreaView.layer.borderWidth = 5.0
        
        let panGestureCropView = UIPanGestureRecognizer(target: self, action: #selector(didMoveCropView(sender:)))
        let pinchGestureCropView = UIPinchGestureRecognizer(target: self, action: #selector(didPinchCropView(sender:)))
//        let rotateGestureCropView = UIRotationGestureRecognizer(target: self, action: #selector(didRotateCropView(sender:)))
        
        
        cropAreaView.addGestureRecognizer(panGestureCropView)
        cropAreaView.addGestureRecognizer(pinchGestureCropView)
//        cropAreaView.addGestureRecognizer(rotateGestureCropView)
    }
    

    @objc func didPinchCropView(sender: UIPinchGestureRecognizer) {
        if (sender.state == .began || sender.state == .changed) {
            
            let transform = (sender.view?.transform)!.scaledBy(x: sender.scale, y: sender.scale)
            sender.view?.transform = transform
            sender.scale = 1
        }
    }
    
//    @objc func didRotateCropView(sender: UIRotationGestureRecognizer) {
//        cropAreaView.transform = cropAreaView.transform.rotated(by: sender.rotation)
//        sender.rotation = 0
//    }
    
    
    @objc func didMoveCropView(sender: UIPanGestureRecognizer ) {
        let translation = sender.translation(in: self.mainImageView)
        cropAreaView.center = CGPoint(x: cropAreaView.center.x + translation.x, y: cropAreaView.center.y + translation.y)
        sender.setTranslation(CGPoint.zero, in: self.mainImageView)
    }

    @IBAction func closeBtnPressed(_ sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    @IBAction func tickBtnPressed(_ segue: UIButton) {
        
        if let croppedCGImage = mainImageView.image?.cgImage?.cropping(to: cropArea) {
            let newImage = UIImage(cgImage: croppedCGImage)
            mainImageView.image = newImage
            delegateImg.passImage(img: mainImageView.image!)
            dismiss(animated: false, completion: nil)
        } else {
            let alert = UIAlertController(title: "Error", message: "Cannot crop the Image !", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    @IBAction func rectBtnPressed(_ sender: UIButton) {
        cropAreaView.layer.cornerRadius = 0
        cropAreaView.frame = CGRect(x: 0, y: 0, width: 200, height: 100)
        cropAreaView.center = mainImageView.center
    }
    @IBAction func ovalBtnPressed(_ sender: UIButton) {
        cropAreaView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        cropAreaView.layer.cornerRadius = cropAreaView.bounds.width / 2
        cropAreaView.layer.masksToBounds = true
        cropAreaView.center = mainImageView.center

    }
    
    @IBAction func rotateBtnPressed(_ sender: UIButton) {
        cropAreaView.transform = cropAreaView.transform.rotated(by: .pi / 2)
        mainImageView.image = mainImageView.image?.rotateImage(withRadian: .pi / 2)
    }
    @IBAction func custom(_ sender: UIButton) {
        cropAreaView.layer.cornerRadius = 0
        cropAreaView.frame = CGRect(x: mainImageView.center.x - cropAreaView.bounds.width / 2, y: mainImageView.center.y - cropAreaView.bounds.height / 2, width: 200, height: 100)
        cropAreaView.center = mainImageView.center
    }
    @IBAction func ratio11(_ sender: UIButton) {
        cropAreaView = resizeView(view: cropAreaView, toRatio: 1)
    }
    @IBAction func ratio23(_ sender: UIButton) {
        cropAreaView = resizeView(view: cropAreaView, toRatio: 2 / 3)
    }
    @IBAction func ratio32(_ sender: UIButton) {
        cropAreaView = resizeView(view: cropAreaView, toRatio: 3 / 2)
    }
    @IBAction func ratio34(_ sender: UIButton) {
        cropAreaView = resizeView(view: cropAreaView, toRatio: 3 / 4)
    }
    
    @IBAction func ratio43(_ sender: UIButton) {
        cropAreaView = resizeView(view: cropAreaView, toRatio: 4 / 3)
    }
    
    @IBAction func ratio916(_ sender: UIButton) {
        cropAreaView = resizeView(view: cropAreaView, toRatio: 9 / 16)
    }
    @IBAction func ratio169(_ sender: UIButton) {
        cropAreaView = resizeView(view: cropAreaView, toRatio: 16 / 9)
    }
    
    func resizeView(view: UIView, toRatio: CGFloat) -> UIView {
        let height:CGFloat = 100
        let width = toRatio * height
        view.frame = CGRect(x: 0, y: 0, width: width, height: 100)
        view.center = mainImageView.center
        cropAreaView.layer.cornerRadius = 0
        return view
    }
    
    func makeRoundImg(img: UIImageView) -> UIImage {
        let imgLayer = CALayer()
        imgLayer.frame = img.bounds
        imgLayer.contents = img.image?.cgImage;
        imgLayer.masksToBounds = true;
        
        imgLayer.cornerRadius = img.frame.size.width/2
        
        UIGraphicsBeginImageContext(img.bounds.size)
        imgLayer.render(in: UIGraphicsGetCurrentContext()!)
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return roundedImage!;
    }
}

