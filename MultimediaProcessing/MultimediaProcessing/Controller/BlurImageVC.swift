//
//  BlurImageVC.swift
//  MultimediaProcessing
//
//  Created by Phong Ngo Phu on 1/18/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit

class BlurImageVC: UIViewController {
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var blurSlider: UISlider!
    
    var passedImage: UIImage!
    var delegateImg : ImagePassingDelegate!
    var blurredImg = UIImage()
    var currentImageSize = CGSize.zero
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainImageView.image = passedImage
        currentImageSize = passedImage.size
        blurDidChangeValue(blurSlider)
    }
    @IBAction func blurDidChangeValue(_ sender: UISlider) {
        let currentValue = sender.value
        
        let inputCIImage = CIImage(image: passedImage)!
        let blurFilter = CIFilter(name: "CIGaussianBlur")!
        blurFilter.setValue(inputCIImage, forKey: kCIInputImageKey)
        blurFilter.setValue(currentValue, forKey: kCIInputRadiusKey)
        let outputImg = UIImage(ciImage: blurFilter.outputImage!)
        
        mainImageView.image = outputImg.resizeImage(targetSize: currentImageSize)
    }

    @IBAction func closeBtnPressed(_ sender: UIButton) {
        delegateImg.passImage(img: passedImage)
        dismiss(animated: false, completion: nil)
    }
    @IBAction func tickBtnPressed(_ sender: UIButton) {
        delegateImg.passImage(img: mainImageView.image!)
        dismiss(animated: false, completion: nil)
    }
}
