//
//  RecentPhotosVC.swift
//  MultimediaProcessing
//
//  Created by Phong Ngo Phu on 1/8/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit
import Photos
class RecentPhotosVC: UIViewController {

    @IBOutlet weak var myTableView: UITableView!
    
    var picker:UIImagePickerController = UIImagePickerController()
    var pickedImageView = UIImageView()
    let cellId = "recentPhotosCell"
    var recentPhotoData = [[Any]]()
    let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
    let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        myTableView.delegate = self
        myTableView.dataSource = self
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        myTableView.reloadData()
        
        checkPhotoPermission()
        checkCameraPermission()
    }

    
    @IBAction func openImageBtnPressed(_ sender: UIButton) {
        if PHPhotoLibrary.authorizationStatus() == .authorized {
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Photo access denied ", message: "Go to setting and grant access to photo !", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            let settings = UIAlertAction(title: "Settings", style: .default, handler: { (_) in
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                }
            })
            alert.addAction(settings)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }

    }
    @IBAction func cameraBtnPressed(_ sender: UIButton) {
        if AVCaptureDevice.authorizationStatus(for: .video) == .authorized {
            openCamera()
        } else {
            let alert = UIAlertController(title: "Camera access denied ", message: "Go to setting and grant access to camera !", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            let settings = UIAlertAction(title: "Settings", style: .default, handler: { (_) in
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                }
            })
            alert.addAction(settings)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
        
    }
    func checkPhotoPermission() {
        switch photoAuthorizationStatus {
        case .authorized: break
        case .notDetermined, .denied, .restricted:
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                if newStatus ==  PHAuthorizationStatus.authorized {
                    print("Granted access to Photo")
                }
            })
        }
    }
    func checkCameraPermission() {
        switch cameraAuthorizationStatus {
        case .authorized: break
        case .notDetermined, .denied, .restricted:
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
                if granted {
                    print("Granted access to camera")
                } else {
                    print("Denied access to camera")
                }
            }
        }
    }
    
    func openCamera() {
        if(UIImagePickerController.isSourceTypeAvailable(.camera)){
            picker.delegate = self
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            present(picker, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
}
extension RecentPhotosVC: RecentImagePassingDelegate {
    func passRecentImage(img: UIImage, date: String) {
        recentPhotoData.append([img,date])
    }
}

extension RecentPhotosVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        pickedImageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        picker.dismiss(animated: false, completion: nil)
        
        guard let imageVC = storyboard?.instantiateViewController(withIdentifier: "toImageVC") as? ImageVC else { return }
        if let myImage = pickedImageView.image {
            imageVC.passedImage = myImage
            imageVC.recentImgDelegate = self
            presentDetail(imageVC)
        }
    }
}

extension RecentPhotosVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recentPhotoData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? RecentPhotosCell else { return UITableViewCell() }
        let img = recentPhotoData[indexPath.row][0] as! UIImage
        let dateCreated = recentPhotoData[indexPath.row][1] as! String
        cell.configureCell(name: dateCreated, image: img)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = myTableView.cellForRow(at: indexPath) as? RecentPhotosCell
        guard let imageVC = storyboard?.instantiateViewController(withIdentifier: "toImageVC") as? ImageVC else { return }
        imageVC.passedImage = (cell?.recentImageView.image)!
        imageVC.recentImgDelegate = self
        presentDetail(imageVC)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(100)
    }
}
