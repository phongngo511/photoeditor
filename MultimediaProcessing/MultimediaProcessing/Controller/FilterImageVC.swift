//
//  FilterImageVC.swift
//  MultimediaProcessing
//
//  Created by Phong Ngo Phu on 1/15/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit
import CoreImage

class FilterImageVC: UIViewController {

    // Outlets
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var filtersScrollView: UIScrollView!
    
    // Variables
    var passedImage = UIImage()
    var arrImage = [UIImage]()
    var delegateImg : ImagePassingDelegate!
    var CIFilterNames = [
        "CIPhotoEffectChrome",
        "CIPhotoEffectFade",
        "CIPhotoEffectInstant",
        "CIPhotoEffectMono",
        "CIPhotoEffectNoir",
        "CIPhotoEffectProcess",
        "CIPhotoEffectTonal",
        "CIPhotoEffectTransfer",
        "CISepiaTone"
    ]
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if arrImage.isEmpty == false {
            arrImage = []
        }
        
        setupUI()
        createFiltedImageForArray()
    }
    
    func setupUI() {
        mainImageView.image = passedImage
        
        var xCoord: CGFloat = 5
        let yCoord: CGFloat = 5
        let buttonWidth:CGFloat = 70
        let buttonHeight: CGFloat = 50
        let gapBetweenButtons: CGFloat = 5
        var itemCount = 0
        
        // Create Default-button
        let defaultButton = UIButton(type: .custom)
        defaultButton.frame = CGRect(x: xCoord, y: yCoord, width: buttonWidth, height: buttonHeight)
        defaultButton.showsTouchWhenHighlighted = true
        defaultButton.addTarget(self, action: #selector(filterButtonTapped(sender:)), for: .touchUpInside)
        defaultButton.layer.cornerRadius = 5
        defaultButton.clipsToBounds = true
        let img = passedImage.resizeImage(targetSize: CGSize(width: 50, height: 50))
        defaultButton.setBackgroundImage(img, for: .normal)
        xCoord += buttonWidth + gapBetweenButtons
        filtersScrollView.addSubview(defaultButton)
        
        // Loop for creating buttons
        for i in 0..<CIFilterNames.count {
            itemCount = i
            
            // Button properties
            let filterButton = UIButton(type: .custom)
            filterButton.frame = CGRect(x: xCoord, y: yCoord, width: buttonWidth, height: buttonHeight)
            filterButton.tag = itemCount + 1
            filterButton.showsTouchWhenHighlighted = true
            filterButton.addTarget(self, action: #selector(filterButtonTapped(sender:)), for: .touchUpInside)
            filterButton.layer.cornerRadius = 5
            filterButton.clipsToBounds = true
            
            // Create filters for each button
            let ciContext = CIContext(options: nil)
            let img = passedImage.resizeImage(targetSize: CGSize(width: 50, height: 50))
            let coreImage = CIImage(image: img)
            let filter = CIFilter(name: "\(CIFilterNames[i])" )!
            filter.setDefaults()
            filter.setValue(coreImage, forKey: kCIInputImageKey)
            let filteredImageData = filter.value(forKey: kCIOutputImageKey) as! CIImage
            let filteredImageRef = ciContext.createCGImage(filteredImageData, from: filteredImageData.extent)
            let imageForButton = UIImage(cgImage: filteredImageRef!)
            filterButton.setBackgroundImage(imageForButton, for: .normal)
            // Add Buttons in the Scroll View
            xCoord +=  buttonWidth + gapBetweenButtons
            filtersScrollView.addSubview(filterButton)
        }
        
        // 75 is default button's width + gap
        filtersScrollView.contentSize = CGSize(width: buttonWidth * CGFloat(itemCount + 2) + 75, height: yCoord)
    }
    func createFiltedImageForArray() {
        arrImage.append(passedImage)
        for i in 0..<CIFilterNames.count {
            let ciContextLiteral = CIContext(options: nil)
            let img = passedImage.resizeImage(targetSize: passedImage.size)
            let coreImageLiteral = CIImage(image: img)
            let filterLiteral = CIFilter(name: "\(CIFilterNames[i])" )!
            filterLiteral.setDefaults()
            filterLiteral.setValue(coreImageLiteral, forKey: kCIInputImageKey)
            let filteredImageDataLiteral = filterLiteral.value(forKey: kCIOutputImageKey) as! CIImage
            let filteredImageRefLiteral = ciContextLiteral.createCGImage(filteredImageDataLiteral, from: filteredImageDataLiteral.extent)
            let imageForArray = UIImage(cgImage: filteredImageRefLiteral!)

            arrImage.append(imageForArray)
        }
    }
    
    @objc func filterButtonTapped(sender: UIButton) {
        let buttonTag = sender.tag
        mainImageView.image = arrImage[buttonTag]
    }
    @IBAction func closeBtnPressed(_ sender: UIButton) {
        delegateImg.passImage(img: passedImage)
        dismiss(animated: false, completion: nil)
    }
    @IBAction func tickBtnPressed(_ sender: UIButton) {
        delegateImg.passImage(img: mainImageView.image!)
        dismiss(animated: false, completion: nil)
    }
    
}
