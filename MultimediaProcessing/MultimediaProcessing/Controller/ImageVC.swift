//
//  ImageVC.swift
//  MultimediaProcessing
//
//  Created by Phong Ngo Phu on 1/7/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit

class ImageVC: UIViewController  {
    // Outlets
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var basicView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    
    
    // Variables
    var isFlipH = false
    var isFlipV = false
    
    var passedImage = UIImage()
    var recentImgDelegate: RecentImagePassingDelegate!
    var currentImageSize = CGSize.zero
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    func setupUI() {
        mainImageView.image = passedImage
        currentImageSize = passedImage.size
        bottomView.isHidden = true
        
        scrollView.delegate = self
        scrollView.isUserInteractionEnabled = false
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        tap.numberOfTapsRequired = 2
        mainImageView.addGestureRecognizer(tap)
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        bottomView.isHidden = true
        saveBtn.isHidden = true
        scrollView.zoomScale = scrollView.minimumZoomScale
        
        scrollView.isUserInteractionEnabled = false
        basicView.isHidden = false
    }
    
    @IBAction func saveBtnPressed(_ sender: UIButton) {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .medium
        let str = formatter.string(from: Date())
        
        UIImageWriteToSavedPhotosAlbum(mainImageView.image! , self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        
        recentImgDelegate.passRecentImage(img: mainImageView.image! ,date: str)
        dismissDetail()
    }
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved !", message: "Your image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                self.dismissDetail()
            }))
            present(ac, animated: true)
        }
    }
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        dismissDetail()
    }
    
    // Mark: - Basic edit
    @IBAction func changePhotoBtnPressed(_ sender: UIButton) {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    @IBAction func flipHBtnPressed(_ sender: UIButton) {
        if isFlipH == false {
            if let img = mainImageView {
                img.image = UIImage(cgImage: (img.image?.cgImage)!, scale: 1.0, orientation: UIImageOrientation.upMirrored)
                mainImageView = img
                isFlipH = !isFlipH
            }

        } else {
            if let img = mainImageView {
                img.image = UIImage(cgImage: (img.image?.cgImage)!, scale: 1.0, orientation: UIImageOrientation.up)
                mainImageView = img
                isFlipH = !isFlipH
            }
        }
    }
    
    @IBAction func flipVBtnPressed(_ sender: UIButton) {
        if isFlipV == false {
            if let img = mainImageView {
                img.image = UIImage(cgImage: (img.image?.cgImage)!, scale: 1.0, orientation: UIImageOrientation.downMirrored)
                mainImageView = img
                isFlipV = !isFlipV
            }
        } else {
            if let img = mainImageView {
                img.image = UIImage(cgImage: (img.image?.cgImage)!, scale: 1.0, orientation: UIImageOrientation.up)
                mainImageView = img
                isFlipV = !isFlipV
            }
        }
    }
    
    @IBAction func zoomInBtnPressed(_ sender: UIButton) {
        mainImageView.transform = mainImageView.transform.scaledBy(x: 1.2, y: 1.2)
    }
    
    @IBAction func zoomOutBtnPressed(_ sender: UIButton) {
        mainImageView.transform = mainImageView.transform.scaledBy(x: 0.8, y: 0.8)
    }
    
    @IBAction func rotateBtnPressed(_ sender: UIButton) {
        mainImageView.transform = mainImageView.transform.rotated(by: .pi / 2)
    }
    
    @IBAction func tickBtnPressed(_ sender: Any) {
        basicView.isHidden = true        
        scrollView.isUserInteractionEnabled = true
        bottomView.isHidden = false
        saveBtn.isHidden = false
        
        mainImageView.image = viewToImage(drawTextAndImageIn: imageView)
        
    }
    
    // Mark: - Edit in Advance
//    func topMostController() -> UIViewController {
//        var topController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
//        while (topController.presentedViewController != nil) {
//            topController = topController.presentedViewController!
//        }
//        return topController
//    }
    @IBAction func cropBtnPressed(_ sender: Any) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "toCropImageVC") as? CropImageVC else { return }
        vc.delegateImg = self
        vc.passedImage = mainImageView.image!
        present(vc, animated: false, completion: nil)
    }
    @IBAction func stickerBtnPressed(_ sender: UIButton) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "toStickerVC") as? StickerVC else { return }
        vc.delegateImg = self
        vc.passedImage = mainImageView.image!
        present(vc, animated: false, completion: nil)
    }
    @IBAction func effectBtnPressed(_ sender: UIButton) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "toEffectVC") as? EffectVC else { return }
        vc.delegateImg = self
        vc.passedImage = mainImageView.image!
        present(vc, animated: false, completion: nil)
    }
    
    @IBAction func filterBtnPressed(_ sender: UIButton) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "toFilterImageVC") as? FilterImageVC else { return }
        vc.delegateImg = self
        vc.passedImage = mainImageView.image!
        present(vc, animated: false, completion: nil)
    }
    
    @IBAction func blurBtnPreseed(_ sender: UIButton) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "toBlurImageVC") as? BlurImageVC else { return }
        vc.delegateImg = self
        vc.passedImage = mainImageView.image!
        present(vc, animated: false, completion: nil)
    }
    @IBAction func textBtnPressed(_ sender: UIButton) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "toTextVC") as? TextVC else { return }
        vc.delegateImg = self
        vc.passedImage = mainImageView.image!
        present(vc, animated: false, completion: nil)
    }
    @IBAction func shareBtnPressed(_ sender: UIButton) {
        if let img = mainImageView.image {
            let vc = UIActivityViewController(activityItems: [img], applicationActivities: [])
            present(vc, animated: true, completion: nil)
        }
    }
    
    func viewToImage(drawTextAndImageIn view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, 0)
        
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
extension ImageVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        mainImageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        picker.dismiss(animated: false, completion: nil)
    }
    
}
extension ImageVC: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return mainImageView
    }
}
extension ImageVC : ImagePassingDelegate {
    func passImage(img: UIImage) {
        mainImageView.image = img
    }
}

